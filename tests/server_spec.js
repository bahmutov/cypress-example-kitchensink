context('Cypress.Server', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/cypress-api/server')
  })

  // **** Server ****
  //
  // Permanently override server options for
  // all instances of cy.server()
  //
  // https://on.cypress.io/api/api-server

  it('Cypress.Server.defaults() - change default config of server', function(){

    Cypress.Server.defaults({
      delay: 0,
      force404: false,
      whitelist: function(xhr){
        // handle custom logic for whitelisting
      }
    })

  })

})
