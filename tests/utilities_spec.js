context('Utilities', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/utilities')
  })

  // **** Utilities ****
  //
  // Cypress offers some utilities commands
  // that give you access to methods from other
  // commonly used libraries

  it('Cypress._.method() - call an underscore method', function(){

    cy
      // use the _.chain, _.pluck, _.first, and _.value functions
      // https://on.cypress.io/api/cypress-underscore
      .request('http://jsonplaceholder.typicode.com/users').then(function(response){
        var _ = Cypress._
        var ids = _.chain(response.body).pluck('id').first(3).value()

        expect(ids).to.deep.eq([1, 2, 3])
      })

  })

  it('Cypress.$(selector) - call a jQuery method', function(){

    // https://on.cypress.io/api/cypress-jquery
    var $li = Cypress.$('.utility-jquery li:first')

    cy
      .wrap($li)
        .should('not.have.class', 'active')
      .click()
        .should('have.class', 'active')

  })


  it('Cypress.moment() - format or parse dates using a moment method', function(){

    // use moment's format function
    // https://on.cypress.io/api/cypress-moment
    var time = Cypress.moment().utc('2014-04-25T19:38:53.196Z').format('h:mm A')

    cy
      .get('.utility-moment').contains('3:38 PM')
        .should('have.class', 'badge')

  })

  it('Cypress.Blob.method() - blob utilties and base64 string conversion', function(){

    // https://on.cypress.io/api/cypress-blob
    // https://github.com/nolanlawson/blob-util#imgSrcToDataURL
    // get the dataUrl string for the javascript-logo
    return Cypress.Blob.imgSrcToDataURL('/assets/img/javascript-logo.png', undefined, 'anonymous')
    .then(function(dataUrl){
      // create an <img> element and set its src to the dataUrl
      var img = Cypress.$('<img />', {src: dataUrl})

      // need to explicitly return cy here since we are initially returning
      // the Cypress.Blog.imgSrcToDataURL promise to our test
      return cy
        .get('.utility-blob').then(function($div){
          // append the image
          $div.append(img)
        })
        .get('.utility-blob img').click().should('have.attr', 'src', dataUrl)
    })

  })

  it('new Cypress.Promise(function) - instantiate a bluebird promise', function(){

    // https://on.cypress.io/api/cypress-promise
    var waited = false

    function waitOneSecond(){
      // return a promise that resolves after 1 second
      return new Cypress.Promise(function(resolve, reject){
        setTimeout(function(){
          // set waited to true
          waited = true

          // resolve with 'foo' string
          resolve('foo')
        }, 1000)
      })
    }

    cy
      .then(function(){
        // return a promise to cy.then() that
        // is awaited until it resolves
        return waitOneSecond().then(function(str){
          expect(str).to.eq('foo')
          expect(waited).to.be.true
        })
      })

  })

})
