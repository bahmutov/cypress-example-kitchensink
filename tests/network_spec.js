context('Network Requests', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/network-requests')
  })

  // **** Network Requests ****
  //
  // Manage AJAX / XHR requests in your app

  it('cy.server() - control the behavior of network requests and responses', function(){

    // https://on.cypress.io/api/server
    cy.server().then(function(server){
      // the default options on server
      // you can override any of these options
      expect(server.delay).to.eq(0)
      expect(server.method).to.eq('GET')
      expect(server.status).to.eq(200)
      expect(server.headers).to.be.null
      expect(server.response).to.be.null
      expect(server.onRequest).to.be.undefined
      expect(server.onResponse).to.be.undefined
      expect(server.onAbort).to.be.undefined

      // These options control the server behavior
      // affecting all requests
      expect(server.enable).to.be.true              // pass false to disable existing route stubs
      expect(server.force404).to.be.false           // forces requests that don't match your routes to 404
      expect(server.whitelist).to.be.a('function')  // whitelists requests from ever being logged or stubbed
    })

    cy
      .server({
        method: 'POST',
        delay: 1000,
        status: 422,
        response: {}
      })

      // any route commands will now inherit the above options
      // from the server. anything we pass specifically
      // to route will override the defaults though.

  })

  it('cy.request() - make an XHR request', function(){

    // https://on.cypress.io/api/request
    cy
      .request('http://jsonplaceholder.typicode.com/comments').then(function(response){
        expect(response.status).to.eq(200)
        expect(response.body).to.have.length(500)
        expect(response).to.have.property('headers')
        expect(response).to.have.property('duration')
      })
  })

  it('cy.route() - route responses to matching requests', function(){

    cy.server()

    // **** GET comments route ****
    //
    // https://on.cypress.io/api/route
    cy
      .route(/comments\/1/).as('getComment')

      // we have code that fetches a comment when
      // the button is clicked in scripts.js
      .get('.network-btn').click()

      // **** Wait ****
      //
      // Wait for a specific resource to resolve
      // continuing to the next command
      //
      // https://on.cypress.io/api/wait
      .wait('@getComment').its('status').should('eq', 200)


    // **** POST comment route ****
    //
    // Specify the route to listen to method 'POST'
    cy
      .route('POST', '/comments').as('postComment')

      // we have code that posts a comment when
      // the button is clicked in scripts.js
      .get('.network-post').click()
      .wait('@postComment')

      // get the route
      .get('@postComment').then(function(xhr){
        expect(xhr.requestBody).to.include('email')
        expect(xhr.requestHeaders).to.have.property('Content-Type')
        expect(xhr.responseBody).to.have.property('name', 'Using POST in cy.route()')
      })


    // **** Stubbed PUT comment route ****
    //
    message = 'whoa, this comment doesn\'t exist'

    cy
      .route({
          method: 'PUT',
          url: /comments\/\d+/,
          status: 404,
          response: {error: message},
          delay: 500
        }).as('putComment')

      // we have code that puts a comment when
      // the button is clicked in scripts.js
      .get('.network-put').click()

      .wait('@putComment')

      // our 404 statusCode logic in scripts.js executed
      .get('.network-put-comment').should('contain', message)

  })

})
