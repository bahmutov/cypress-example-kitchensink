context('Window', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/window')
  })

  // **** Window ****
  //
  // Cypress has commands to help you get
  // access to window, document, and title

  it('cy.window() - get the global window object', function(){

    // https://on.cypress.io/api/window
    cy.window().should('have.property', 'top')

  })

  it('cy.document() - get the document object', function(){

    // https://on.cypress.io/api/document
    cy.document().should('have.property', 'charset').and('eq', 'UTF-8')

  })

  it('cy.title() - get the title', function(){

    // https://on.cypress.io/api/title
    cy.title().should('include', 'Kitchen Sink')

  })
})
