context('Cypress.Dom', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/cypress-api/dom')
  })

  // **** Dom ****
  //
  // Cypress.Dom holds methods and logic related to DOM.
  //
  // https://on.cypress.io/api/dom

  it('Cypress.Dom.isHidden() - determine if a DOM element is hidden', function(){

    var hiddenP = Cypress.$('.dom-p p.hidden').get(0)
    var visibleP = Cypress.$('.dom-p p.visible').get(0)

    // our first paragraph has css class 'hidden'
    expect(Cypress.Dom.isHidden(hiddenP)).to.be.true
    expect(Cypress.Dom.isHidden(visibleP)).to.be.false

  })

})