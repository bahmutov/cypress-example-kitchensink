context('Connectors', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/connectors')
  })

  // **** Connectors ****
  //
  // Some commands are just used to manipulate elements,
  // properties or invoke functions on the current subject

  it('cy.each() - iterate over an array of elements', function(){

    // https://on.cypress.io/api/each

    cy
      .get('.connectors-each-ul>li')
      .each(function($el, index, $list){
        console.log($el, index, $list)
      })
  })

  it('cy.its() - get properties on the current subject', function(){

    // https://on.cypress.io/api/its
    cy
      .get('.connectors-its-ul>li')
      // calls the 'length' property returning that value
        .its('length')
          .should('be.gt', 2)
  })

  it('cy.invoke() - invoke a function on the current subject', function(){

    // our div is hidden in our script.js
    // $('.connectors-div').hide()

    // https://on.cypress.io/api/invoke
    cy
      .get('.connectors-div').should('be.hidden')

      // call the jquery method 'show' on the 'div.container'
      .invoke('show')
        .should('be.visible')

  })

  it('cy.spread() - spread an array as individual arguments to a callback function', function(){

    // https://on.cypress.io/api/spread
    var arr = ['foo', 'bar', 'baz']

    cy.wrap(arr).spread(function(foo, bar, baz){
      expect(foo).to.eq('foo')
      expect(bar).to.eq('bar')
      expect(baz).to.eq('baz')
    })

  })

  it('cy.then() - invoke a callback function with the current subject', function(){

    // https://on.cypress.io/api/then
    cy.get('.connectors-list>li').then(function($lis){
      expect($lis).to.have.length(3)
      expect($lis.eq(0)).to.contain('Walk the dog')
      expect($lis.eq(1)).to.contain('Feed the cat')
      expect($lis.eq(2)).to.contain('Write JavaScript')
    })

  })

})