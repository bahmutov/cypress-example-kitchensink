context('Cypress.Cookies', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/cypress-api/cookies')
  })

  // **** Cookies ****
  //
  // Manage your app's cookies while testing
  //
  // https://on.cypress.io/api/cookies

  it('Cypress.Cookies.debug() - enable or disable debugging', function(){

    Cypress.Cookies.debug(true)

    // Cypress will now log in the console when
    // cookies are set or cleared
    cy.setCookie('fakeCookie', '123ABC')
    cy.clearCookie('fakeCookie')
    cy.setCookie('fakeCookie', '123ABC')
    cy.clearCookie('fakeCookie')
    cy.setCookie('fakeCookie', '123ABC')

  })

  it('Cypress.Cookies.preserveOnce() - preserve cookies by key', function(){

    // normally cookies are reset after each test
    cy.getCookie('fakeCookie').should('not.be.ok')

    // preserving a cookie will not clear it when
    // the next test starts
    cy.setCookie('lastCookie', '789XYZ')
    Cypress.Cookies.preserveOnce('lastCookie')

  })

  it('Cypress.Cookies.defaults() - set defaults for all cookies', function(){

    // now any cookie with the name 'session_id' will
    // not be cleared before each new test runs
    Cypress.Cookies.defaults({
      whitelist: 'session_id'
    })

  })

})