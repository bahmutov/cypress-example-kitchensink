context('Assertions', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/assertions')
  })
  // **** Assertions ****
  //
  describe('Implicit Assertions', function(){

    it('cy.should - make an assertion about the current subject', function(){

      // https://on.cypress.io/api/should
      cy
        .get('.assertion-table')
          .find('tbody tr:last').should('have.class', 'success')

    })

    it('cy.and - chain multiple assertions together', function(){

      // https://on.cypress.io/api/and
      cy
        .get('.assertions-link')
          .should('have.class', 'active')
          .and('have.attr', 'href')
          .and('include', 'cypress.io')

    })

  })

  describe('Explicit Assertions', function(){

    it('expect - make an assertion about a specified subject', function(){

      // We can use Chai's BDD style assertions
      expect(true).to.be.true

      // Pass a function to should that can have any number
      // of explicit assertions within it.
      cy
        .get('.assertions-p').find('p')
        .should(function($p){
          // return an array of texts from all of the p's
          var texts = $p.map(function(i, el){
            // https://on.cypress.io/api/cypress-jquery
            return Cypress.$(el).text()
          })

          // jquery map returns jquery object
          // and .get() convert this to simple array
          var texts = texts.get()

          // array should have length of 3
          expect(texts).to.have.length(3)

          // set this specific subject
          expect(texts).to.deep.eq([
            'Some text from first p',
            'More text from second p',
            'And even more text from third p'
          ])
      })

    })

  })

})