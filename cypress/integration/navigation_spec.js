context('Navigation', function(){
  beforeEach(function(){
    cy
      .visit('https://example.cypress.io')
      .get('.navbar-nav').contains('Commands').click()
      .get('.dropdown-menu').contains('Navigation').click()
  })

  // **** Navigation ****
  //
  // We can issue commands to visit, reload the page,
  // navigate in the browser's history

  it('cy.go() - go back or forward in the browser\'s history', function(){

    cy.location().its('pathname').should('include', 'navigation')

    // https://on.cypress.io/api/go
    cy.go('back')
    cy.location().its('pathname').should('not.include', 'navigation')


    cy.go('forward')
    cy.location().its('pathname').should('include', 'navigation')

    // equivalent to clicking back
    cy.go(-1)
    cy.location().its('pathname').should('not.include', 'navigation')


    // equivalent to clicking forward
    cy.go(1)
    cy.location().its('pathname').should('include', 'navigation')

  })

  it('cy.reload() - reload the page', function(){

    // https://on.cypress.io/api/reload
    cy.reload()

    // reload the page without using the cache
    cy.reload(true)

  })

  it('cy.visit() - visit a remote url', function(){

    // Visit any url
    // https://on.cypress.io/api/visit
    cy.visit('http://www.warnerbros.com/archive/spacejam/movie/jam.htm')

    // Pass options to the visit
    cy.visit('https://example.cypress.io/commands/navigation', {
      timeout: 50000, // increase total time for the visit to resolve
      onBeforeLoad: function(contentWindow){
        // contentWindow is the remote page's window object
      },
      onLoad: function(contentWindow){
        // contentWindow is the remote page's window object
      }
    })
  })

})