context('Cookies', function(){
  beforeEach(function(){
    Cypress.Cookies.debug(true)

    cy.visit('https://example.cypress.io/commands/cookies')

    // clear cookies again after visiting to remove
    // any 3rd party cookies picked up such as cloudflare
    .clearCookies()
  })

  it('cy.getCookie() - get a browser cookie', function(){

    // **** Get a Cookie ****
    //
    // // https://on.cypress.io/api/getcookie
    cy
      .get('#getCookie .set-a-cookie').click()

      // getCookie() returns a cookie object
      .getCookie('token').should('have.property', 'value', '123ABC')
  })

  it('cy.getCookies() - get browser cookies', function(){

    // **** Get all Cookies ****
    //
    // // https://on.cypress.io/api/getcookies
    cy
      .getCookies().should('be.empty')

      .get('#getCookies .set-a-cookie').click()

      // getCookies() returns an array of cookies
      .getCookies().should('have.length', 1).then( function(cookies) {

        // each cookie has these properties
        expect(cookies[0]).to.have.property('name', 'token')
        expect(cookies[0]).to.have.property('value', '123ABC')
        expect(cookies[0]).to.have.property('httpOnly', false)
        expect(cookies[0]).to.have.property('secure', false)
        expect(cookies[0]).to.have.property('domain')
        expect(cookies[0]).to.have.property('path')

      })
  })

  it('cy.setCookie() - set a browser cookie', function(){

    // **** Set a Cookie ****
    //
    // // https://on.cypress.io/api/setcookie
    cy
      .getCookies().should('be.empty')

      .setCookie('foo', 'bar')

      // getCookie() returns a cookie object
      .getCookie('foo').should('have.property', 'value', 'bar')
  })

  it('cy.clearCookie() - clear a browser cookie', function(){

    // **** Clear a Cookie ****
    //
    // // https://on.cypress.io/api/clearcookie
    cy
      .getCookie('token').should('be.null')

      .get('#clearCookie .set-a-cookie').click()

      .getCookie('token').should('have.property', 'value', '123ABC')

      // clearCookies() returns null
      .clearCookie('token').should('be.null')

      .getCookie('token').should('be.null')
  })

  it('cy.clearCookies() - clear browser cookies', function(){

    // **** Clear all Cookies ****
    //
    // https://on.cypress.io/api/clearcookies

    cy
      .getCookies().should('be.empty')

      .get('#clearCookies .set-a-cookie').click()

      .getCookies().should('have.length', 1)

      // clearCookies() returns null
      .clearCookies()

      .getCookies().should('be.empty')

  })

})