context('Misc', function(){
  beforeEach(function(){
    cy.visit('https://example.cypress.io/commands/misc')
  })

  it('cy.end() - end the command chain', function(){

    // cy.end is useful when you want to end a chain of commands
    // and force Cypress to re-query from the root element
    //
    // https://on.cypress.io/api/end
    cy
      .get('.misc-table').within(function(){
        cy
          // ends the current chain and returns null
          .contains("Cheryl").click().end()

          // queries the entire table again
          .contains("Charles").click()

      })
  })

  it('cy.exec() - execute a system command', function(){

    // cy.exec allows you to execute a system command.
    // so you can take actions necessary for your test,
    // but outside the scope of Cypress.
    //
    // https://on.cypress.io/api/exec
    cy
      .exec('echo Jane Lane')
        .its('stdout').should('contain', 'Jane Lane')

      .exec('cat cypress.json')
        .its('stderr').should('be.empty')

      .exec('pwd')
        .its('code').should('eq', 0)
  })

  it('cy.focused() - get the DOM element that has focus', function(){

    // https://on.cypress.io/api/focused
    cy
      .get('.misc-form').find('#name').click()
      .focused().should('have.id', 'name')

      .get('.misc-form').find('#description').click()
      .focused().should('have.id', 'description')

  })

  it("cy.screenshot() - take a screenshot", function(){

    // https://on.cypress.io/api/screenshot
    cy.screenshot("my-image")
  })

  it('cy.wrap() - wrap an object', function(){

    // https://on.cypress.io/api/wrap
    cy
      .wrap({foo: 'bar'})
        .should('have.property', 'foo')
        .and('include', 'bar')

  })

})